﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace SkyrimSaveGameManager {
	class SkyrimSaveException : Exception {
		public SkyrimSaveException()
			: base("Unknown reading error occured.")
		{
		}

		public SkyrimSaveException(String msg)
			: base(msg)
		{

		}

		public SkyrimSaveException(String msg, Exception inner)
			: base(msg, inner)
		{

		}
	}

	class SkyrimReader : BinaryReader {
		public SkyrimReader(Stream input) : base(input)
		{

		}

		public String ReadWString()
		{
			UInt16 length = ReadUInt16();
			char[] str = ReadChars(length);
			return new String(str);
		}
	}

	class SkyrimSave {
		private const string MAGIC_CONSTANT = "TESV_SAVEGAME";
		private String file_loc;
		private int pc;
		private SkyrimReader bin;

		private String player_name;
		public String PlayerName {
			get { return player_name; }
		}

		private UInt32 player_level;
		public UInt32 PlayerLevel {
			get { return player_level; }
		}

		private String player_loc;
		public String PlayerLoc {
			get { return player_loc; }
		}

		private String game_date;
		public String GameDate {
			get { return game_date; }
		}

		private String player_race;
		public String PlayerRace {
			get { return player_race; }
		}

		private String file_time;
		public String FileTime {
			get { return file_time; }
		}

		private Bitmap screenshot;
		public Bitmap Screenshot {
			get { return screenshot; }
		}

		public SkyrimSave(String file_loc)
		{
			this.file_loc = file_loc;
		}

		public void parse_save()
		{
			FileStream save = File.OpenRead(file_loc);
			bin = new SkyrimReader(save);

			String magic = new String(bin.ReadChars(13));
			if (!MAGIC_CONSTANT.Equals(magic)) {
				throw new SkyrimSaveException("Invalid magic constant.");
			}

			UInt32 header_size = bin.ReadUInt32();

			// for whatever fucking reason, the endianess of this one is different from the others //
			UInt32 version = bin.ReadByte();
			bin.ReadBytes(3);

			UInt32 save_number = bin.ReadUInt32();
			player_name = bin.ReadWString();
			player_level = bin.ReadUInt32();
			player_loc = bin.ReadWString();
			game_date = bin.ReadWString();
			player_race = bin.ReadWString();

			// sex and experience are skipped //
			bin.ReadUInt16();
			bin.ReadInt32();
			bin.ReadInt32();

			file_time = DateTime.FromFileTime((long)bin.ReadUInt64()).ToString();

			UInt32 width = bin.ReadUInt32();
			UInt32 height = bin.ReadUInt32();
			parse_screenshot(width, height);

			System.Console.Out.WriteLine("Parsed save file successfully!");

			bin.Close();
			save.Close();
		}

		private void parse_screenshot(UInt32 width, UInt32 height)
		{
			screenshot = new Bitmap((int)width, (int)height, PixelFormat.Format24bppRgb);
			UInt32 length = 3 * width * height;
			for (UInt32 y = 0; y < height; y++) {
				for (UInt32 x = 0; x < width; x++) {
					int r = bin.ReadByte();
					int g = bin.ReadByte();
					int b = bin.ReadByte();
					Color c = Color.FromArgb(r, g, b);
					screenshot.SetPixel((int)x, (int)y, c);
				}
			}	
		}
	}
}
