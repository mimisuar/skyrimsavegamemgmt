﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using InputBoxForm;

namespace SkyrimSaveGameManager
{
	public partial class MainForm : Form
	{
		private String save_folder_path;
		private Settings settings;

		public MainForm()
		{
			InitializeComponent();

			settings = new Settings();

			save_folder_path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			save_folder_path += "\\My Games\\Skyrim\\Saves\\";

			import_save_data();
		}

		private bool safe_copy(String fname)
		{
			try {
				File.Copy(save_folder_path + SaveList.Text, save_folder_path + fname);
			}
			catch (System.NotSupportedException) {
				MessageBox.Show("Invalid filename: " + fname);
				return false;
			}
			catch (Exception exc) {
				MessageBox.Show(exc.Message);
				return false;
			}

			return true;
		}

		public void import_save_data()
		{
			IEnumerable<String> saves = Directory.EnumerateFiles(save_folder_path);

			bool selected = false;
			foreach (String save in saves)
			{
				// rip out the filename itself //
				int last_index_of_slash = save.LastIndexOf("\\");
				String savefile_name = save.Substring(last_index_of_slash + 1);

				if (save.Contains(".ess") && !(save.Contains(".bak") || save.Contains(".skse"))) {
					SaveList.Items.Add(savefile_name);
					if (!selected) { selected = true; SaveList.SelectedIndex = 0; }
				}
			}

			if (SaveList.Items.Count == 0) {
				MessageBox.Show("No save games detected. Aborting.");
				this.Close();
			}
		}

		void export_screenshot()
		{
			SaveFileDialog save_pic = new SaveFileDialog();
			save_pic.FileName = SaveList.Text;
			save_pic.Filter = "BMP|*.bmp";
			save_pic.AddExtension = true;

			save_pic.ShowDialog();

			if (!save_pic.FileName.Contains(".bmp")) {
				save_pic.FileName += ".bmp";
			}

			Screenshot.Image.Save(save_pic.FileName);
		}

		void rename(String oldname, String newname)
		{
			File.Copy(oldname, newname);
			File.Delete(oldname);
		}

		void rename_save()
		{
			InputBoxResult res = InputBox.Show("New file name: ",
				"Rename Save", SaveList.Text, null);
			if (res.OK && !res.Text.Equals("")) {
				if (!res.Text.Contains(".ess")) {
					res.Text += ".ess";
				}

				if (!safe_copy(res.Text)) {
					return;
				}

				File.Delete(save_folder_path + SaveList.Text);
				SaveList.Items.Remove(SaveList.Text);
				SaveList.Items.Add(res.Text);
				SaveList.SelectedIndex = SaveList.Items.IndexOf(res.Text);
			}
		}

		void create_backup()
		{
			InputBoxResult res = InputBox.Show("New file name: ",
				"Backup Save", SaveList.Text, null);
			if (res.OK && !res.Text.Equals("")) {
				if (!res.Text.Contains(".ess")) {
					res.Text += ".ess";
				}

				if (!res.Text.Contains(".bak")) {
					res.Text += ".bak";
				}

				if (!safe_copy(res.Text)) {
					return;
				}

				MessageBox.Show("Backup created as " + res.Text);
			}
		}

		void load_backup()
		{
			OpenFileDialog open_bak = new OpenFileDialog();
			open_bak.Filter = "Skyrim Save Backup|*.bak";
			open_bak.InitialDirectory = save_folder_path;
			open_bak.ShowDialog();
			String oldname = open_bak.FileName;
			String newname = oldname.Substring(0, oldname.LastIndexOf(".bak"));
			rename(oldname, newname);
			SaveList.Items.Add(newname.Substring(newname.LastIndexOf("\\") + 1));
		}

		void delete_save()
		{
			DialogResult dr = MessageBox.Show("Delete " + SaveList.Text + "?", "Delete Save",
				MessageBoxButtons.YesNo);

			if (dr == DialogResult.Yes) {
				File.Delete(save_folder_path + SaveList.Text);
				SaveList.Items.Remove(SaveList.Text);
				MessageBox.Show("Save file deleted.");
			}
		}

		void update_labels(SkyrimSave save)
		{
			player_name.Text = save.PlayerName;
			player_level.Text = "Level " + save.PlayerLevel.ToString();
			player_loc.Text = save.PlayerLoc;
			player_race.Text = save.PlayerRace;
			game_date.Text = save.GameDate;
			file_time.Text = save.FileTime;
		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			About about_box = new About();
			about_box.Show();
		}

		private void SaveList_SelectedIndexChanged(object sender, EventArgs e)
		{
			SkyrimSave save = new SkyrimSave(save_folder_path + SaveList.Text);
			save.parse_save();
			Screenshot.Image = save.Screenshot;
			Screenshot.Update();
			update_labels(save);
		}

		private void quitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		

		private void renameToolStripMenuItem_Click(object sender, EventArgs e)
		{
			rename_save();
		}

		private void backupToolStripMenuItem_Click(object sender, EventArgs e)
		{
			create_backup();
		}

		private void exportScreenshotToolStripMenuItem_Click(object sender, EventArgs e)
		{
			export_screenshot();
		}

		private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
		{
			delete_save();
		}

		private void loadToolStripMenuItem_Click(object sender, EventArgs e)
		{
			load_backup();
		}

		private void Screenshot_Click(object sender, EventArgs e)
		{
			//export_screenshot();
		}

		private void player_race_Click(object sender, EventArgs e)
		{

		}

		private void prefsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Prefs prefs = new Prefs(settings);
			prefs.ShowDialog();
		}

		private void skyrimLauncherToolStripMenuItem_Click(object sender, EventArgs e)
		{
			System.Console.Out.WriteLine("Launching Skyrim Launcher...");
			System.Diagnostics.Process.Start(settings.skyrim_launcher_path);
		}

		private void skyrimToolStripMenuItem_Click(object sender, EventArgs e)
		{
			System.Console.Out.WriteLine("Launching Skyrim...");
			System.Diagnostics.Process.Start(settings.skyrim_path);
		}

		private void modOrganizerToolStripMenuItem_Click(object sender, EventArgs e)
		{
			System.Console.Out.WriteLine("Launching Mod Organizer...");
			System.Diagnostics.Process.Start(settings.mod_path);
		}
	}
}
