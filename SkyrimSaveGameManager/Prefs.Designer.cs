﻿namespace SkyrimSaveGameManager {
	partial class Prefs {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.skyrim_path_box = new System.Windows.Forms.TextBox();
			this.skyrim_launcher_path_box = new System.Windows.Forms.TextBox();
			this.mod_path_box = new System.Windows.Forms.TextBox();
			this.skyrim_path_button = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.save_button = new System.Windows.Forms.Button();
			this.cancel_button = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(65, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(69, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Skyrim Path: ";
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(20, 46);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(114, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Skyrim Launcher Path:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(30, 80);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(104, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Mod Organizer Path:";
			// 
			// skyrim_path_box
			// 
			this.skyrim_path_box.Location = new System.Drawing.Point(140, 10);
			this.skyrim_path_box.Name = "skyrim_path_box";
			this.skyrim_path_box.Size = new System.Drawing.Size(100, 20);
			this.skyrim_path_box.TabIndex = 3;
			this.skyrim_path_box.TextChanged += new System.EventHandler(this.skyrim_path_box_TextChanged);
			// 
			// skyrim_launcher_path_box
			// 
			this.skyrim_launcher_path_box.Location = new System.Drawing.Point(140, 43);
			this.skyrim_launcher_path_box.Name = "skyrim_launcher_path_box";
			this.skyrim_launcher_path_box.Size = new System.Drawing.Size(100, 20);
			this.skyrim_launcher_path_box.TabIndex = 4;
			this.skyrim_launcher_path_box.TextChanged += new System.EventHandler(this.skyrim_launcher_path_box_TextChanged);
			// 
			// mod_path_box
			// 
			this.mod_path_box.Location = new System.Drawing.Point(140, 77);
			this.mod_path_box.Name = "mod_path_box";
			this.mod_path_box.Size = new System.Drawing.Size(100, 20);
			this.mod_path_box.TabIndex = 5;
			this.mod_path_box.TextChanged += new System.EventHandler(this.mod_path_box_TextChanged);
			// 
			// skyrim_path_button
			// 
			this.skyrim_path_button.Location = new System.Drawing.Point(246, 8);
			this.skyrim_path_button.Name = "skyrim_path_button";
			this.skyrim_path_button.Size = new System.Drawing.Size(26, 23);
			this.skyrim_path_button.TabIndex = 6;
			this.skyrim_path_button.Text = "...";
			this.skyrim_path_button.UseVisualStyleBackColor = true;
			this.skyrim_path_button.Click += new System.EventHandler(this.skyrim_path_button_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(246, 41);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(26, 23);
			this.button1.TabIndex = 7;
			this.button1.Text = "...";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(246, 75);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(26, 23);
			this.button2.TabIndex = 8;
			this.button2.Text = "...";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// save_button
			// 
			this.save_button.Location = new System.Drawing.Point(33, 122);
			this.save_button.Name = "save_button";
			this.save_button.Size = new System.Drawing.Size(75, 23);
			this.save_button.TabIndex = 9;
			this.save_button.Text = "Save";
			this.save_button.UseVisualStyleBackColor = true;
			this.save_button.Click += new System.EventHandler(this.save_button_Click);
			// 
			// cancel_button
			// 
			this.cancel_button.Location = new System.Drawing.Point(165, 122);
			this.cancel_button.Name = "cancel_button";
			this.cancel_button.Size = new System.Drawing.Size(75, 23);
			this.cancel_button.TabIndex = 10;
			this.cancel_button.Text = "Cancel";
			this.cancel_button.UseVisualStyleBackColor = true;
			this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
			// 
			// Prefs
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 157);
			this.Controls.Add(this.cancel_button);
			this.Controls.Add(this.save_button);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.skyrim_path_button);
			this.Controls.Add(this.mod_path_box);
			this.Controls.Add(this.skyrim_launcher_path_box);
			this.Controls.Add(this.skyrim_path_box);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "Prefs";
			this.Text = "Prefrences";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox skyrim_path_box;
		private System.Windows.Forms.TextBox skyrim_launcher_path_box;
		private System.Windows.Forms.TextBox mod_path_box;
		private System.Windows.Forms.Button skyrim_path_button;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button save_button;
		private System.Windows.Forms.Button cancel_button;
	}
}