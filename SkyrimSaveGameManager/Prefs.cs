﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SkyrimSaveGameManager {
	
	public partial class Prefs : Form {
		private Settings settings;

		public Prefs(Settings settings)
		{
			InitializeComponent();
			this.settings = settings;
			skyrim_launcher_path_box.Text = settings.skyrim_launcher_path;
			skyrim_path_box.Text = settings.skyrim_path;
			mod_path_box.Text = settings.mod_path;
		}

		private void label1_Click(object sender, EventArgs e)
		{

		}

		private String get_exe()
		{
			OpenFileDialog open_exe = new OpenFileDialog();
			open_exe.Filter = "Executable|*.exe";
			open_exe.Multiselect = false;
			open_exe.ShowDialog();
			return open_exe.FileName;
		}

		private void skyrim_path_button_Click(object sender, EventArgs e)
		{
			skyrim_path_box.Text = get_exe();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			skyrim_launcher_path_box.Text = get_exe();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			mod_path_box.Text = get_exe();
		}

		private void mod_path_box_TextChanged(object sender, EventArgs e)
		{
			settings.mod_path = mod_path_box.Text;
		}

		private void skyrim_launcher_path_box_TextChanged(object sender, EventArgs e)
		{
			settings.skyrim_launcher_path = skyrim_launcher_path_box.Text;
		}

		private void skyrim_path_box_TextChanged(object sender, EventArgs e)
		{
			settings.skyrim_path = skyrim_path_box.Text;
		}

		private void cancel_button_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void save_button_Click(object sender, EventArgs e)
		{
			settings.save();
			MessageBox.Show("Prefrences saved!");
			this.Close();
		}
	}

	public class Settings {
		public String skyrim_path;
		public String skyrim_launcher_path;
		public String mod_path;
		private const String config = "ssm.config";

		public Settings()
		{
			skyrim_path = "";
			skyrim_launcher_path = "";
			mod_path = "";

			if (File.Exists(config)) {
				load();
			}
		}

		public void load()
		{
			String[] lines = File.ReadAllLines(config);
			if (lines.Length < 3) {
				MessageBox.Show("Unable to load settings.");
				return;
			}
			skyrim_path = lines[0];
			skyrim_launcher_path = lines[1];
			mod_path = lines[2];
			System.Console.Out.WriteLine("Prefrences loaded.");
		}

		public void save()
		{
			String[] lines = new String[3];
			lines[0] = skyrim_path;
			lines[1] = skyrim_launcher_path;
			lines[2] = mod_path;

			File.WriteAllLines(config, lines);
		}


	}
}
