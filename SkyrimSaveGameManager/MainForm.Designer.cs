﻿namespace SkyrimSaveGameManager
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.SaveList = new System.Windows.Forms.ListBox();
			this.Screenshot = new System.Windows.Forms.PictureBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exportScreenshotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.backupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.createBackupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.player_name = new System.Windows.Forms.Label();
			this.player_race = new System.Windows.Forms.Label();
			this.player_level = new System.Windows.Forms.Label();
			this.player_loc = new System.Windows.Forms.Label();
			this.game_date = new System.Windows.Forms.Label();
			this.file_time = new System.Windows.Forms.Label();
			this.launchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.skyrimLauncherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.skyrimToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.modOrganizerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.prefsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.Screenshot)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// SaveList
			// 
			this.SaveList.FormattingEnabled = true;
			this.SaveList.Location = new System.Drawing.Point(12, 27);
			this.SaveList.Name = "SaveList";
			this.SaveList.Size = new System.Drawing.Size(300, 277);
			this.SaveList.Sorted = true;
			this.SaveList.TabIndex = 0;
			this.SaveList.SelectedIndexChanged += new System.EventHandler(this.SaveList_SelectedIndexChanged);
			// 
			// Screenshot
			// 
			this.Screenshot.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.Screenshot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Screenshot.ErrorImage = null;
			this.Screenshot.Location = new System.Drawing.Point(318, 27);
			this.Screenshot.Name = "Screenshot";
			this.Screenshot.Size = new System.Drawing.Size(335, 219);
			this.Screenshot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.Screenshot.TabIndex = 1;
			this.Screenshot.TabStop = false;
			this.Screenshot.Click += new System.EventHandler(this.Screenshot_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.launchToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(665, 24);
			this.menuStrip1.TabIndex = 2;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.prefsToolStripMenuItem,
            this.quitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F9;
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// quitToolStripMenuItem
			// 
			this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
			this.quitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
			this.quitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.quitToolStripMenuItem.Text = "Quit";
			this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renameToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.exportScreenshotToolStripMenuItem,
            this.backupsToolStripMenuItem});
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
			this.saveToolStripMenuItem.Text = "Save";
			// 
			// renameToolStripMenuItem
			// 
			this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
			this.renameToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
			this.renameToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
			this.renameToolStripMenuItem.Text = "Rename...";
			this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
			// 
			// deleteToolStripMenuItem
			// 
			this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
			this.deleteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
			this.deleteToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
			this.deleteToolStripMenuItem.Text = "Delete...";
			this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
			// 
			// exportScreenshotToolStripMenuItem
			// 
			this.exportScreenshotToolStripMenuItem.Name = "exportScreenshotToolStripMenuItem";
			this.exportScreenshotToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.exportScreenshotToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
			this.exportScreenshotToolStripMenuItem.Text = "Export Screenshot...";
			this.exportScreenshotToolStripMenuItem.Click += new System.EventHandler(this.exportScreenshotToolStripMenuItem_Click);
			// 
			// backupsToolStripMenuItem
			// 
			this.backupsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createBackupToolStripMenuItem,
            this.loadToolStripMenuItem});
			this.backupsToolStripMenuItem.Name = "backupsToolStripMenuItem";
			this.backupsToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
			this.backupsToolStripMenuItem.Text = "Backups";
			// 
			// createBackupToolStripMenuItem
			// 
			this.createBackupToolStripMenuItem.Name = "createBackupToolStripMenuItem";
			this.createBackupToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
			this.createBackupToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
			this.createBackupToolStripMenuItem.Text = "Create...";
			this.createBackupToolStripMenuItem.Click += new System.EventHandler(this.backupToolStripMenuItem_Click);
			// 
			// loadToolStripMenuItem
			// 
			this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
			this.loadToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.B)));
			this.loadToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
			this.loadToolStripMenuItem.Text = "Load...";
			this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
			// 
			// player_name
			// 
			this.player_name.AutoSize = true;
			this.player_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.player_name.Location = new System.Drawing.Point(314, 249);
			this.player_name.Name = "player_name";
			this.player_name.Size = new System.Drawing.Size(75, 20);
			this.player_name.TabIndex = 3;
			this.player_name.Text = "Prisoner";
			// 
			// player_race
			// 
			this.player_race.AutoSize = true;
			this.player_race.Location = new System.Drawing.Point(315, 276);
			this.player_race.Name = "player_race";
			this.player_race.Size = new System.Drawing.Size(33, 13);
			this.player_race.TabIndex = 4;
			this.player_race.Text = "Race";
			this.player_race.Click += new System.EventHandler(this.player_race_Click);
			// 
			// player_level
			// 
			this.player_level.AutoSize = true;
			this.player_level.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.player_level.Location = new System.Drawing.Point(594, 269);
			this.player_level.Name = "player_level";
			this.player_level.Size = new System.Drawing.Size(59, 20);
			this.player_level.TabIndex = 5;
			this.player_level.Text = "Level 1";
			// 
			// player_loc
			// 
			this.player_loc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.player_loc.Location = new System.Drawing.Point(395, 268);
			this.player_loc.Name = "player_loc";
			this.player_loc.Size = new System.Drawing.Size(193, 23);
			this.player_loc.TabIndex = 6;
			this.player_loc.Text = "Location";
			this.player_loc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// game_date
			// 
			this.game_date.AutoSize = true;
			this.game_date.Location = new System.Drawing.Point(318, 291);
			this.game_date.Name = "game_date";
			this.game_date.Size = new System.Drawing.Size(61, 13);
			this.game_date.TabIndex = 7;
			this.game_date.Text = "Game Date";
			// 
			// file_time
			// 
			this.file_time.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.file_time.Location = new System.Drawing.Point(398, 291);
			this.file_time.Name = "file_time";
			this.file_time.Size = new System.Drawing.Size(190, 13);
			this.file_time.TabIndex = 8;
			this.file_time.Text = "File Time";
			this.file_time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// launchToolStripMenuItem
			// 
			this.launchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.skyrimLauncherToolStripMenuItem,
            this.skyrimToolStripMenuItem,
            this.modOrganizerToolStripMenuItem});
			this.launchToolStripMenuItem.Name = "launchToolStripMenuItem";
			this.launchToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
			this.launchToolStripMenuItem.Text = "Launch";
			// 
			// skyrimLauncherToolStripMenuItem
			// 
			this.skyrimLauncherToolStripMenuItem.Name = "skyrimLauncherToolStripMenuItem";
			this.skyrimLauncherToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
			this.skyrimLauncherToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
			this.skyrimLauncherToolStripMenuItem.Text = "Skyrim Launcher";
			this.skyrimLauncherToolStripMenuItem.Click += new System.EventHandler(this.skyrimLauncherToolStripMenuItem_Click);
			// 
			// skyrimToolStripMenuItem
			// 
			this.skyrimToolStripMenuItem.Name = "skyrimToolStripMenuItem";
			this.skyrimToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
			this.skyrimToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
			this.skyrimToolStripMenuItem.Text = "Skyrim";
			this.skyrimToolStripMenuItem.Click += new System.EventHandler(this.skyrimToolStripMenuItem_Click);
			// 
			// modOrganizerToolStripMenuItem
			// 
			this.modOrganizerToolStripMenuItem.Name = "modOrganizerToolStripMenuItem";
			this.modOrganizerToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
			this.modOrganizerToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
			this.modOrganizerToolStripMenuItem.Text = "Mod Organizer";
			this.modOrganizerToolStripMenuItem.Click += new System.EventHandler(this.modOrganizerToolStripMenuItem_Click);
			// 
			// prefsToolStripMenuItem
			// 
			this.prefsToolStripMenuItem.Name = "prefsToolStripMenuItem";
			this.prefsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.prefsToolStripMenuItem.Text = "Prefs...";
			this.prefsToolStripMenuItem.Click += new System.EventHandler(this.prefsToolStripMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(665, 310);
			this.Controls.Add(this.file_time);
			this.Controls.Add(this.game_date);
			this.Controls.Add(this.player_loc);
			this.Controls.Add(this.player_level);
			this.Controls.Add(this.player_race);
			this.Controls.Add(this.player_name);
			this.Controls.Add(this.Screenshot);
			this.Controls.Add(this.SaveList);
			this.Controls.Add(this.menuStrip1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Skyrim Save Manager";
			((System.ComponentModel.ISupportInitialize)(this.Screenshot)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox SaveList;
		private System.Windows.Forms.PictureBox Screenshot;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exportScreenshotToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem backupsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem createBackupToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
		private System.Windows.Forms.Label player_name;
		private System.Windows.Forms.Label player_race;
		private System.Windows.Forms.Label player_level;
		private System.Windows.Forms.Label player_loc;
		private System.Windows.Forms.Label game_date;
		private System.Windows.Forms.Label file_time;
		private System.Windows.Forms.ToolStripMenuItem prefsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem launchToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem skyrimLauncherToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem skyrimToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem modOrganizerToolStripMenuItem;
	}
}

